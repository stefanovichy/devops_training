FROM node:latest
COPY hello.js /helloapp/hello.js
EXPOSE 8081
ENTRYPOINT ["node", "/helloapp/hello.js"]
